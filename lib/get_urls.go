package lib

import (
	"context"

	"golang.org/x/sync/errgroup"
	"golang.org/x/sync/semaphore"

	"gitlab.com/the-xeptore/snapp-food-coding-challenge/c/config"
)

func GetUrls(config *config.Config, urls []string) ([]string, error) {
	responses := make([]string, len(urls))
	var g errgroup.Group
	ctx := context.Background()
	var sem = semaphore.NewWeighted(int64(config.Concurrency))

	for index, url := range urls {
		sem.Acquire(ctx, 1)
		index, url := index, url
		g.Go(func() error {
			defer sem.Release(1)
			resp, err := getUrl(config.RequestTimeoutInSeconds, url)
			if nil != err {
				return err
			}

			responses[index] = resp
			return nil
		})
	}

	return responses, g.Wait()
}
