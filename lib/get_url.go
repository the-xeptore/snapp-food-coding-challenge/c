package lib

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"
)

func getUrl(timeoutInSeconds uint64, url string) (string, error) {
	req, err := http.NewRequest("GET", url, nil)
	if nil != err {
		log.Println("Error reading request for url:", url, err.Error())
		return "", err
	}

	client := &http.Client{Timeout: time.Second * time.Duration(timeoutInSeconds)}

	resp, err := client.Do(req)
	if nil != err {
		log.Println("Error getting response from url: ", url, err.Error())
		return "", err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if nil != err {
		log.Println("Error reading response from url: ", url, err.Error())
		return "", err
	}

	return fmt.Sprintf("%s", body), nil
}
