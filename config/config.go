package config

import (
	"os"

	"gopkg.in/yaml.v2"
)

type Config struct {
	RequestTimeoutInSeconds uint64 `yaml:"request_timeout_in_seconds"`
	Concurrency             int    `yaml:"concurrency"`
}

func ReadConfig() (*Config, error) {
	file, err := os.Open("config.yaml")
	if nil != err {
		return nil, err
	}
	defer file.Close()

	cfg := &Config{}
	decoder := yaml.NewDecoder(file)
	err = decoder.Decode(&cfg)
	if nil != err {
		return nil, err
	}

	return cfg, nil
}
