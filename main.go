package main

import (
	"fmt"
	"log"

	"gitlab.com/the-xeptore/snapp-food-coding-challenge/c/config"
	"gitlab.com/the-xeptore/snapp-food-coding-challenge/c/lib"
)

var urls []string = []string{
	"https://jsonplaceholder.typicode.com/todos/1",
	"https://restcountries.eu/rest/v2/alpha/co",
	"https://jsonplaceholder.typicode.com/todos/3",
}

func main() {
	conf, err := config.ReadConfig()
	if nil != err {
		log.Fatalln("Error reading config file:", err)
	}

	responses, err := lib.GetUrls(conf, urls)
	if nil != err {
		log.Fatalln("Error retrieving urls:", err.Error())
	}

	for index, response := range responses {
		fmt.Printf("> %d: %s", index, response)
		fmt.Println()
	}
}
