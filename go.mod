module gitlab.com/the-xeptore/snapp-food-coding-challenge/c

go 1.16

require (
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c
	gopkg.in/yaml.v2 v2.4.0
)
